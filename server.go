// chrome network secton request / response  ---- www.aait.edu.et
//curl

//     install https://curl.haxx.se/windows/
//     apt install curl
//     brew install curl

// GET		curl http://www.example.com
// POST		curl -d “data to post” http://www.example.com
// PUT		curl -T file1 http://example.com/new/res/file0
// HEAD		curl -I http://www.example.com                  -requests the headers that are returned if the specified resource would be requested with an HTTP GET method

//postman walk through - request - response - headers -authentication - enviroment - collection - publishing documentation
//online docs https://explore.postman.com/

//swagger api documentation

// connecting to mongodb

package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
)

var templ = template.Must(template.ParseFiles("index.html", "about.html"))

func indexHandler(w http.ResponseWriter, r *http.Request) {
	key := "name"
	//value := r.URL.Query().Get(key)
	value := r.FormValue(key)
	templ.Execute(w, value)
}

func aboutHandler(w http.ResponseWriter, r *http.Request) {
	key := "name"
	name := r.FormValue(key)
	templ.ExecuteTemplate(w, "about.html", name)
}

func photoHandler(w http.ResponseWriter, r *http.Request) {
	key := "photo"

	file, handler, err := r.FormFile(key)
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()

	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	r.ParseMultipartForm(10 << 20)
	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	tempFile, err := ioutil.TempFile("assets/uploads", "upload-*.png")
	if err != nil {
		fmt.Println(err)
	}
	defer tempFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)
	// return that we have successfully uploaded our file!
	fmt.Fprintf(w, "Successfully Uploaded File\n")

}

func main() {
	mux := http.NewServeMux()

	fs := http.FileServer(http.Dir("assets"))
	mux.Handle("/assets/", http.StripPrefix("/assets/", fs))

	mux.HandleFunc("/", indexHandler)
	mux.HandleFunc("/about", aboutHandler)
	mux.HandleFunc("/photo", photoHandler)

	http.ListenAndServe(":8080", mux)
}
